# module_name, package_name, ClassName, method_name,
# ExceptionName, function_name, GLOBAL_CONSTANT_NAME,
# global_var_name, instance_var_name, function_parameter_name,
# local_var_name

import serial
import settings
import time


class CarConrtol:

    motor_speed = "\x01\x00\x00"
    helm_angle = "\x02\x00\x00"

    ser = serial.Serial(
        port='/dev/ttyACM0',
        baudrate=115200,
        dsrdtr=1,
        timeout=0,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS
    )
    time.sleep(1)

    def change_car_parameters(self,new_helm_angle, new_motor_speed, key):


        helm_angle_array=map(ord, new_helm_angle)
        motor_speed_array = map(ord, new_motor_speed)
        # helm cases
        if helm_angle_array[1]==0 or helm_angle_array[2]==0:
            if key == 65361:  # left
                helm_angle_array[1] = 1
                helm_angle_array[2] += 1
            if key == 65363:  # right
                helm_angle_array[1] = 2
                helm_angle_array[2] += 1
        elif helm_angle_array[1]==1:
            if key == 65361:  # left
                helm_angle_array[2] += 1
            if key == 65363:  # right
                helm_angle_array[2] -= 1
        elif helm_angle_array[1] == 2:
            if key == 65361:  # left
                helm_angle_array[2] -= 1
            if key == 65363:  # right
                helm_angle_array[2] += 1

        # motor cases
        if motor_speed_array[1]==0 and motor_speed_array[2]==0:

            if key == 65362:  # forward
                motor_speed_array[1] = 1
                motor_speed_array[2] += 1
            elif key == 65364: # backward
                motor_speed_array[1] = 2
                motor_speed_array[2] += 1
        elif motor_speed_array[1]==1 and motor_speed_array[2]==1:
            if key == 65362:  # forward
                motor_speed_array[2] += 1
            elif key == 65364: # backward
                motor_speed_array[1] = 0
                motor_speed_array[2] -= 1

        elif motor_speed_array[1]==2 and motor_speed_array[2]==1:
            if key == 65362:  # forward
                motor_speed_array[1] = 0
                motor_speed_array[2] -= 1
            elif key == 65364: # backward
                motor_speed_array[2] += 1
        elif motor_speed_array[2]>5 and motor_speed_array[1]==2:
            if key == 65362:  # forward
                motor_speed_array[2] -= 1
            if key == 65364:  # forward
                pass
        elif motor_speed_array[2] > 5 and motor_speed_array[1] == 1:
            if key == 65362:  # forward
                pass
            if key == 65364:  # forward
                motor_speed_array[2] -= 1

        else:
            if motor_speed_array[1] == 1:
                if key == 65362:
                    motor_speed_array[2] += 1
                elif key == 65364:  # backward
                    motor_speed_array[2] -= 1
            elif motor_speed_array[1] == 2:
                if key == 65362:
                    motor_speed_array[2] -= 1
                elif key == 65364:  # backward
                    motor_speed_array[2] += 1


        y=''.join('{:02x}'.format(y) for y in helm_angle_array)
        new_helm_angle = y.decode("hex")

        x=''.join('{:02x}'.format(x) for x in motor_speed_array)
        new_motor_speed = x.decode("hex")

        self.helm_angle = new_helm_angle
        self.motor_speed = new_motor_speed

        return self.helm_angle,self.motor_speed

    def set_car_parameters(self, helm_angle_cmd, motor_speed_cmd):

        self.ser.write(motor_speed_cmd)
        self.ser.write(helm_angle_cmd)
        # self.ser.close()

    def get_car_parameters(self):
        return self.helm_angle ,self.motor_speed

    def setup(self):
        serial_settings=settings.settings['serial']

        return serial_settings
