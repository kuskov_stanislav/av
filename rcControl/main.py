#!/usr/bin/env python
import random

import  rcControl
import cv2 as cv
import numpy as np

def main():
    imgFull=np.random.randint(5, size=(100, 100)) #randome ing for waitKey working

    car_control_obj = rcControl.CarConrtol()

    while 1:
        key=cv.waitKey(1)

        cv.imshow("full", imgFull)


        if key == 27:  # Esc key to stop
            motor_speed = "\x01\x00\x00"
            helm_angle = "\x02\x00\x00"
            car_control_obj.set_car_parameters(helm_angle,motor_speed)
            break
        elif key == -1:  # normally -1 returned,so don't print it
            continue
        else:
            # print key
            if key == 32: # space (stop)
                motor_speed = "\x01\x00\x00"
                helm_angle = "\x02\x00\x00"
                car_control_obj.set_car_parameters(helm_angle, motor_speed)
            if key == 65362: #forward ^
                helm_angle,motor_speed=car_control_obj.get_car_parameters() #get current parameters
                helm_angle, motor_speed=car_control_obj.change_car_parameters(helm_angle,motor_speed,key) # change current parameters
                car_control_obj.set_car_parameters(helm_angle, motor_speed) # set new parameters after changing
            if key == 65364: #backward v
                helm_angle, motor_speed = car_control_obj.get_car_parameters()  # get current parameters
                helm_angle, motor_speed = car_control_obj.change_car_parameters(helm_angle, motor_speed,key)  # change current parameters
                car_control_obj.set_car_parameters(helm_angle, motor_speed)  # set new parameters after changing
            if key == 65361: #left >
                helm_angle, motor_speed = car_control_obj.get_car_parameters()  # get current parameters
                helm_angle, motor_speed = car_control_obj.change_car_parameters(helm_angle, motor_speed,key)  # change current parameters
                car_control_obj.set_car_parameters(helm_angle, motor_speed)  # set new parameters after changing
            if key == 65363: #right <
                helm_angle, motor_speed = car_control_obj.get_car_parameters()  # get current parameters
                helm_angle, motor_speed = car_control_obj.change_car_parameters(helm_angle, motor_speed,key)  # change current parameters
                car_control_obj.set_car_parameters(helm_angle, motor_speed)  # set new parameters after changing

if __name__ == "__main__":
    main()