#include <Servo.h> 
int helm=2;
int motor = 3;

int ch2=4;
int ch4=5;
int ch5=6;
int ch6=7;
int ch7=10;
int ch8=11;
int pin_result=12;


Servo motor_left;
Servo helm_servo;

int helm_center=90;
int helm_angle_pin = 8;
int mot_pin_left = 9;

int complete_cmd[8];

int helm_angle_rc;
int motor_speed_rc;

const int PeriodMs = 20;
unsigned long TimerMs = PeriodMs;

int motor_speed,helm_angle,ch2_val,ch4_val,ch5_val,ch6_val,ch7_val,ch8_val;

void setup()
{
    Serial.begin(115200);    //Start serial at baud rate 9600
    pinMode(motor, INPUT);  //Define motor and helm as input
    pinMode(helm,INPUT);
    
    pinMode(ch2,INPUT);  //Define other channels as input
    pinMode(ch4,INPUT);
    pinMode(ch5,INPUT); 
    pinMode(ch6,INPUT);
 //   pinMode(ch7,INPUT); 
 //   pinMode(ch8,INPUT);
    
    motor_left.attach(mot_pin_left);    //Инициальзация левого мотора (порт, начальная позиция, максимальная позиция)       !!!
    helm_servo.attach(helm_angle_pin);
}
void loop()
{
  
  // Определение значения счетчика милллисекунд
// от начала запуска программы
unsigned long timeMs = millis();

if ( timeMs >= TimerMs ) {
  
  motor_speed = pulseIn(motor, HIGH);            //Read the pulse and store it as val
  helm_angle = pulseIn(helm,HIGH);                   //Print val to serial monitor
  
  ch2_val = pulseIn(ch2,HIGH);            //Read the pulse and store it as val
  ch4_val = pulseIn(ch4,HIGH);                   //Print val to serial monitor
  ch5_val = pulseIn(ch5,HIGH);            //Read the pulse and store it as val
  ch6_val = pulseIn(ch6,HIGH);                   //Print val to serial monitor
  //ch7_val = pulseIn(ch7,HIGH);            //Read the pulse and store it as val
  //ch8_val = pulseIn(ch8,HIGH); 
  
  complete_cmd[0]=motor_speed;
  complete_cmd[1]=helm_angle;
  complete_cmd[2]=ch2_val;
  complete_cmd[3]=ch4_val;
  complete_cmd[4]=ch5_val;
  complete_cmd[5]=ch6_val;
  complete_cmd[6]=ch7_val;
  complete_cmd[7]=ch8_val;
  
  //complete_cmd[6]=1000;
  //complete_cmd[7]=1000;
  
  motor_speed_rc = map(motor_speed, 900, 1800,1000, 1900);
  helm_angle_rc = map(helm_angle, 900, 1800, -25, 25);
  
  //Serial.println("\t");
  //Serial.println("\t");
  //Serial.print(motor_speed_rc);
  //Serial.print("\t");
  //Serial.print(helm_angle_rc);

  
  Serial.println("\t");
  Serial.print(TimerMs,10);
//  for (int i=0; i <= 7; i++){
//    Serial.print("\t");
//    Serial.print(complete_cmd[i]);
//   } 
   int count=0;
   int summ=0;
   
   for (int i=0;i<=7;i++){
     
    digitalWrite(pin_result, LOW);
    delayMicroseconds(200);       
    digitalWrite(pin_result, HIGH);
    delayMicroseconds(complete_cmd[i]);
     
     summ+=complete_cmd[i]+200;
     
     if (i==7){
       count=1;
     }

   }
   digitalWrite(pin_result, LOW);
   delayMicroseconds(200); 
   
   digitalWrite(pin_result, HIGH);
   delayMicroseconds(PeriodMs*1000-summ);
   
   //след поток (действия с count)
   
   
//  if (motor_speed_rc < 1600){ 
//    if (motor_speed_rc > 1400){
//      motor_speed_rc=1500;
//    }
//  }
//  if (helm_angle_rc<2){ 
//    if (helm_angle_rc>-2){
//      helm_angle_rc=0;
//    }
//  
//  }
  
  motor_left.write(motor_speed_rc);
  helm_servo.write(helm_center+helm_angle_rc);
  
  
TimerMs = timeMs + PeriodMs;
  
}


}
