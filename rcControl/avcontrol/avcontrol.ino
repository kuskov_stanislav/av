#include <Servo.h> 


//pins for RC
int helm=2;
int motor = 3;
int ch2=4;
int ch4=5;
int ch5=6;
int ch6=7;

Servo motor_left;
Servo helm_servo;

int helm_angle_pin = 8;
int mot_pin_left = 9;

int js_position = 1500;  //Начальная позиция, всегда 1.5 мс для регуляторов бесколлекторных двигателей
int max_position = 2300; //Максимальное значение ШИМ 2.3 мс
int min_position = 800;  //Минимальное значени ШИМ 0.8 мс

int start = 1;  //Флаг задержки запуска

helm_angle_step=5;

char cmd_for_car;
int motor_backward_array[]={
0,1390,1380,1370,1360,1350,1340
};
int motor_forward_array[]={
0,1600,1610,1620,1630,1640,1650
};
int helm_angle_array[]={
0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
};

int i;

String motor_speed;
String helm_angle;
int x;
int helm_center=90;

void setup() {   
Serial.begin(115200);  
motor_left.attach(mot_pin_left, js_position, max_position);    //Инициальзация левого мотора (порт, начальная позиция, максимальная позиция)       !!!
helm_servo.attach(helm_angle_pin);
helm_servo.write(90);

}

void loop() {
  
int i=0;
char buffer[100];

  if (Serial.available() > 0) {
    delay(10);
    
    while( Serial.available() && i< 6) {      
        buffer[i++] = Serial.read();
     }
     //закрываем массив
     buffer[i++]='\0';   
           int motor = buffer[0];
           int motor_direction = buffer[1];
           int motor_value = buffer[2];
           // buffer for helm
           int helm = buffer [3];
           int helm_direction=buffer[4];
           int helm_value=buffer[5];
           
           if(start == 1) {
             motor_left.write(js_position); 
             delay(1500);
             start = 0;
           }
                    
           
           if (int(motor) == 1){
             //Serial.println(motor);             
             if (int(motor_direction) == 1){
               motor_left.write(motor_forward_array[motor_value]); //speed
               
               //send data to motor
             } else if (int(motor_direction) == 2){           
               motor_left.write(motor_backward_array[motor_value]); //speed
                //send data to notor               
             } else if (int(motor_direction) == 0) {
               motor_left.write(js_position);
               delay(1000);                           
             }
           }           
           if (int(helm) == 2){
             if (int(helm_direction)==1){
               helm_servo.write(helm_center-helm_value*helm_angle_step);
             }
             if (int(helm_direction)==2){
               helm_servo.write(helm_center+helm_value*helm_angle_step);
             }
               if (int(helm_direction)==0){
               helm_servo.write(helm_center);
             }             
           }
  }
  
    
}

